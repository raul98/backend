exports.set = function(App){
    App.get('/', (request, response) => {
        response.send('<h1>Hola, Mundo</h1>');
    });
    App.use('/user', require('./sources/user/user.routes'));
    App.use('/user-session', require('./sources/session/user_session.routes'));
    App.use('/chat', require('./sources/chat/chat.routes'));
}