const User = require('./user_session.model');
const Sequelize = require('../../sequelize');
const UserSession = require('../session/user_session.model');
const sequelize = require('../../sequelize');
const JWT = require('../../JWT');
const Op = require('sequelize').Op;
const SocketIO = require('./../../socket-io');


exports.getList = function(params, response){
    Sequelize.query(`
    SELECT u.id, u.name, u.username 
    FROM user_session us JOIN users u ON u.id = us.user_id
    `,
    {type: Sequelize.QueryTypes.SELECT})
    .then(data => {
        response.status(200).send(data);
    })
    .catch(error => response.status(400).send(error));
}

exports.create = function(userId, token, socketId){
    return new Promise((resolve, reject) =>{
/*         UserSession.create({
            user_id: userId,
            token,
            socket_id: socketId
        }).then(() => resolve())
        .catch((error) => reject(error)); */
        UserSession.findOrCreate({
            where: {
                token: {[Op.eq] : token}
            },
            defaults:{
                user_id : userId,
                token: token,
                socket_id: socketId
            }
        }).spread((item, created) => {
            if(!created){
                item.user_id = userId;
                item.token = token;
                item.socket_id = socketId;

                item.save()
                    .then(data => response.status(200).send(data))
                    .catch(error => response.status(500).send(error))
            }else  response.status(200).send(true)
        });
    });
}
exports.delete = function(userId){
    return new Promise((resolve, reject) =>{
        UserSession.destroy({
            where : {
                user_id: {[Op.eq] : userId}
            }
        }).then(() => resolve())
        .catch((error) => reject(error));
    });
}
exports.deleteSocket = function(socketId){
    return new Promise((resolve, reject) =>{
        UserSession.destroy({
            where : {
                socket_id: {[Op.eq] : socketId}
            }
        }).then(() => resolve())
        .catch((error) => reject(error));
    });
}


exports.updateSocket = function({query: {id, token}},response){
    UserSession.findOne({
        where: {
            token: {[Op.eq]: token}
        }
    }).then(item => {
        item.socket_id = id;
        item.save().then().catch(error => response.status(500).send(error))
    }).catch(error => response.status(500).send(error));
}

exports.register = function({query:{token}}, response){
    console.log('__________________________________');
    var {id} = JWT.decodeToken(token);
    exports.create(id, token).then(() => {
        response.status(200).send();
        SocketIO.emiters.user_connected();
    }).catch(error => response.status(500).send(error));
}