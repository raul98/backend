const Sequelize = require('sequelize');
const schema = require('../../sequelize')

const User = require('./../user/user.model');


module.exports = schema.define('chat',{
    user_id: {type: Sequelize.INTEGER, allowNull: false},
    text:{type: Sequelize.TEXT, allowNull: false },
    text_time: {type: Sequelize.TEXT, allowNull: true}
}, {
    timestamps: false,
    freezeTableName: true,
    underscored: true
})

//module.exports = Chat.belongsTo(User)