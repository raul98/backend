const Sequelize = require('sequelize');
const schema = require('../../sequelize')


module.exports = schema.define('Users',{
    name: {type: Sequelize.STRING, allowNull: false},
    username: {type: Sequelize.STRING, allowNull: false},
    password: {type: Sequelize.STRING, allowNull: false}
}, {
    timestamps: false,
    freezeTableName: true
})